#!/bin/bash
for i in {1..33} # broj ispitnih primjera
do
# generiraj ime direktorija s vodećom nulom
dir=$(printf "%0*d\n" 2 $i)
echo "Test $dir"
# pokreni program i provjeri izlaz
res=`python SimEnka.py < primjeri/test$dir/test.a | diff primjeri/test$dir/test.b -`
if [ "$res" != "" ]
then
# izlazi ne odgovaraju
echo "FAIL"
echo $res
else
# OK!
echo "OK"
fi
done
