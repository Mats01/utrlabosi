import sys

# inicijalizacija globalnih varijabli koje ce funkcije populirat iz prvih 5 redova ulaza
input_array = []
rijecnik_stanja = {}
abeceda = []
prihvatljiva_stanja = []
pocetno_stanje = None
ordering_alphabet = {}


def parse_input(ulazni_niz):
    global input_array
    ulazni_niz = ulazni_niz[:-1] # mice new line character
    input_array = ulazni_niz.split("|")
    for i in range(len(input_array)):
        input_array[i] = input_array[i].split(",")

        

def parse_state(ulazni_niz):
    global rijecnik_stanja
    ulazni_niz = ulazni_niz[:-1] # mice new line character
    lista_stanja = ulazni_niz.split(",")
    for i in lista_stanja:
        rijecnik_stanja[i] = []

    j = 0
    for i in lista_stanja:
        ordering_alphabet[i] = j
        j += 1
    
def parse_alphabet(ulazni_niz):
    global abeceda
    global ordering_alphabet
    ulazni_niz = ulazni_niz[:-1] # mice new line character
    abeceda = ulazni_niz.split(",")
    abeceda.append("$")
    for i in rijecnik_stanja:
        for j in abeceda:
            rijecnik_stanja[i].append([])
    
    


    

def parse_acceptable(ulazni_niz):
    global prihvatljiva_stanja
    ulazni_niz = ulazni_niz[:-1] # mice new line character
    prihvatljiva_stanja =  ulazni_niz.split(",")

def funkcija(ulaz):
    global input_array
    global rijecnik_stanja
    global abeceda
    global prihvatljiva_stanja
    global pocetno_stanje
    
    try:
        ulaz = ulaz[:-1].split("->")
        lijevi_dio = ulaz[0]
        slijed_stanja = ulaz[1].split(",")
        tren_stanje, simbol = lijevi_dio.split(",")
        for slijedece_stanje in slijed_stanja:
            if slijedece_stanje != "#":
                rijecnik_stanja[tren_stanje][abeceda.index(simbol)].append(slijedece_stanje)
        
    except:
        print("")
    
def predi_u_stanje(stanje, simbol):
    q = []
    q .extend(rijecnik_stanja[stanje][abeceda.index(simbol)])
    slijedaca_stanja = []
    
    while q != []:
        trenutno_stanje = q.pop(0)
        if trenutno_stanje not in slijedaca_stanja:
            slijedaca_stanja.append(trenutno_stanje)
        for i in rijecnik_stanja[trenutno_stanje][abeceda.index("$")]:
            if i not in slijedaca_stanja and i not in q:
                q.append(i)
    

    
    return slijedaca_stanja


def black_box():
    for input_set in input_array:
    
        output = ""
        pocetna_stanja = [pocetno_stanje]
        pocetna_stanja.extend(predi_u_stanje(pocetno_stanje, "$"))
        pocetna_stanja = sorted(pocetna_stanja, key=lambda word: [ordering_alphabet.get(word)])

        
        for i in pocetna_stanja:
            output += i + ","
        output = output[:-1]
        output += "|"


        pocetak = pocetna_stanja

        for simbol in input_set:
            
            
            nova_stanja = []
            for stanje in pocetak:
                
                
                nova_stanja.extend(predi_u_stanje(stanje, simbol))
                nova_stanja = list(dict.fromkeys(nova_stanja))
            
            
            nova_stanja = sorted(nova_stanja, key=lambda word: [ordering_alphabet.get(word)])

            if nova_stanja != []:
                for i in range(len(nova_stanja)-1):
                    output += nova_stanja[i] + ","
                output += nova_stanja[-1]
            else:
                output += "#"
            output += "|"
            pocetak = nova_stanja
            


        output = output[:-1]
        print(output)

  


line_number = 0
for line in sys.stdin:
    line_number += 1
    if line_number == 1:
        parse_input(line)

    elif line_number == 2:
        parse_state(line)

    elif line_number == 3:
        parse_alphabet(line)
    
    elif line_number == 4:
        parse_acceptable(line)
    
    elif line_number == 5:
        pocetno_stanje = line[:-1]

    else:
        funkcija(line)
    
black_box()