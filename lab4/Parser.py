import sys


"""
gramatika
S → aAB | bBA
A → bC | a
B → ccSbc | ϵ
C → AA

"""
deied = False

output_str = ""

ulazni_niz = "aa"


ulaz = None

ulazni_niz = list(ulazni_niz)


def A():
    global output_str
    global ulaz
    global deied

    output_str += "A"

    try:
        ulaz = ulazni_niz.pop(0)
    except IndexError:
        print(output_str)
        deied = True
        print("NE")
        return

    if ulaz == 'b':
        C()
        if deied:
            return
    elif ulaz == 'a':
        return
    else:
        print(output_str)
        deied = True
        print("NE")
        return


def B():
    global output_str
    global deied
    global ulaz

    output_str += "B"

    try:
        ulaz = ulazni_niz.pop(0)
    except IndexError:
        return

    if ulaz == 'c':
        ulaz = ulazni_niz.pop(0)
        if not ulaz == 'c':
            print(output_str)
            deied = True

            print("NE")
            return
        S()
        if deied:
            return

        ulaz = ulazni_niz.pop(0)
        if not ulaz == 'b':
            print(output_str)
            deied = True
            print("NE")
            return

        ulaz = ulazni_niz.pop(0)
        if not ulaz == 'c':
            print(output_str)

            deied = True
            print("NE")
            return

    else:
        # ipak nisam nista procitao jer je ovo bio epsilon prijlaz
        ulazni_niz.insert(0, ulaz)


def C():
    global output_str
    global ulaz
    output_str += "C"

    A()
    if deied:
        return
    A()


def S():
    global output_str
    global ulaz
    ulaz = ulazni_niz.pop(0)

    output_str += "S"
    if ulaz == 'a':
        A()
        if deied:
            return
        B()

    elif ulaz == 'b':
        B()
        if deied:
            return
        A()


for line in sys.stdin:
    ulazni_niz = line[:-1]

ulazni_niz = list(ulazni_niz)

S()

if not deied and not ulazni_niz:

    print(output_str)
    print("DA")

if not deied and ulazni_niz:
    print(output_str)
    print("NE")
