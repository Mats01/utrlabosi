#!/bin/bash
correct=0
for i in {1..25} # broj ispitnih primjera
do
# generiraj ime direktorija s vodećom nulom
dir=$(printf "%0*d\n" 2 $i)
echo "Test $dir"
# pokreni program i provjeri izlaz
res=`python3 SimPa.py < primjeri/test$dir/primjer.in | diff primjeri/test$dir/primjer.out -`
if [ "$res" != "" ]
then
# izlazi ne odgovaraju
echo "FAIL"
echo $res
else
# OK!
echo "OK"
correct=$((correct+1))
fi
done
echo ""
echo "Result $correct/25 ($((correct*100/25))%)"

