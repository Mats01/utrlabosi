import sys
from pprint import pprint
import copy
import re


input_array = []
rijecnik_stanja = {}
"""
primjer izlgeda rijecnika stanja:
{
    'q1': {
        'K': {
            'a': {
                'slijedece': 'q1',
                'stanja_stoga': ['K']
                }

            }
        }
}
"""
abeceda = []
stanja_stoga = []
prihvatljiva_stanja = []
pocetno_stanje = None
pocetni_znak_stoga = None


class PorisniAutomat:

    def __init__(self, ulazni_niz, stog=[], stanje=None, path=""):
        self.ulazni_niz = ulazni_niz
        self.stog = stog
        self.procitani_znak = None
        self.stanje = stanje
        self.path = path

    def __str__(self):
        moj_stog = "".join(self.stog[::-1])
        if not moj_stog:
            moj_stog = "$"
        return "%s#%s" % (self.stanje, moj_stog)

    def print_state(self, string):
        self.path += string + "|"

    def procitaj_znak(self):
        if not self.ulazni_niz:
            return False
        self.procitani_znak = self.ulazni_niz.pop(0)  # actually pop
        return True

    def slijedece_stanje(self):
        if not self.stog:
            self.print_state("fail")
            self.stanje = "fail"
            return False
        if not self.procitaj_znak():
            return False  # ovo se nebi trebalo desit jer provjeravam ranije

        prijelazni_rijecnik = rijecnik_stanja[self.stanje][self.stog[-1]
                                                           ][self.procitani_znak]

        if not prijelazni_rijecnik:
            self.print_state("fail")
            self.stanje = "fail"
            return False

        self.stog.pop()
        self.stanje = prijelazni_rijecnik['slijedece']

        self.stog.extend(prijelazni_rijecnik['stanja_stoga'])

        self.print_state(str(self))

        return True

    def imam_ep_prelaz(self):
        try:
            if rijecnik_stanja[self.stanje][self.stog[-1]]['$']:
                return True
            else:
                return False

        except:
            return False

    def izvedi_ep_prelaz(self):
        prijelazni_rijecnik = rijecnik_stanja[self.stanje][self.stog.pop(
        )]['$']
        self.stanje = prijelazni_rijecnik['slijedece']
        self.stog.extend(prijelazni_rijecnik['stanja_stoga'])

        self.print_state(str(self))


def parse_input(ulazni_niz):
    global input_array
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    input_array = ulazni_niz.split("|")
    for i in range(len(input_array)):
        input_array[i] = input_array[i].split(",")


def parse_state(ulazni_niz):
    global rijecnik_stanja
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    lista_stanja = ulazni_niz.split(",")
    for i in lista_stanja:
        rijecnik_stanja[i] = {}


def parse_alphabet(ulazni_niz):
    global abeceda
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    abeceda = ulazni_niz.split(",")
    abeceda.append("$")


def parse_stack(ulazni_niz):
    global stanja_stoga
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    stanja_stoga = ulazni_niz.split(",")

    for stanje in rijecnik_stanja.values():
        for stanje_stoga in stanja_stoga:

            stanje[stanje_stoga] = {}
            for slovo in abeceda:
                stanje[stanje_stoga][slovo] = None


def parse_acceptable(ulazni_niz):
    global prihvatljiva_stanja
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    prihvatljiva_stanja = ulazni_niz.split(",")


def funkcija(ulaz):
    global input_array
    global rijecnik_stanja
    global abeceda
    global prihvatljiva_stanja
    global pocetno_stanje

    try:
        ulaz = ulaz[:-1].split("->")
        lijevi_dio = ulaz[0]
        novo_stanje, niz_stoga = ulaz[1].split(
            ",")

        niz_stoga = niz_stoga.replace('$', '')  # makni epsilon prelaze
        niz_stoga = list([char for char in niz_stoga])[::-1]  # reverse

        tren_stanje, ulazni_znak, znak_stoga = lijevi_dio.split(",")

        rijecnik_stanja[tren_stanje][znak_stoga][ulazni_znak] = {'slijedece': novo_stanje,
                                                                 'stanja_stoga': niz_stoga}

    except:
        print("")


def the_box_is_orange_actually(ulazni_niz):

    prvi_pa = PorisniAutomat(
        ulazni_niz, [pocetni_znak_stoga], pocetno_stanje)

    prvi_pa.print_state(str(prvi_pa))
    while(prvi_pa.ulazni_niz):
        while(prvi_pa.imam_ep_prelaz()):

            prvi_pa.izvedi_ep_prelaz()

            if prvi_pa.stanje in prihvatljiva_stanja and not prvi_pa.ulazni_niz:
                break

            continue

        if not prvi_pa.slijedece_stanje():

            while (prvi_pa.imam_ep_prelaz()):

                prvi_pa.izvedi_ep_prelaz()

            break

        if prvi_pa.stanje in prihvatljiva_stanja and not prvi_pa.ulazni_niz:
            break

        while (prvi_pa.imam_ep_prelaz()):

            prvi_pa.izvedi_ep_prelaz()
            if prvi_pa.stanje in prihvatljiva_stanja and not prvi_pa.ulazni_niz:
                break

            continue

    output_string = ""
    prihvatljivost = 0

    if prvi_pa.stanje in prihvatljiva_stanja and not prvi_pa.ulazni_niz:
        prihvatljivost = 1

    output_string = "%s%d" % (prvi_pa.path, prihvatljivost)

    print(output_string)


line_number = 0
for line in sys.stdin:
    line_number += 1
    if line_number == 1:
        parse_input(line)

    elif line_number == 2:
        parse_state(line)

    elif line_number == 3:
        parse_alphabet(line)

    elif line_number == 4:
        parse_stack(line)

    elif line_number == 5:
        parse_acceptable(line)

    elif line_number == 6:
        pocetno_stanje = line[:-1]

    elif line_number == 7:
        pocetni_znak_stoga = line[:-1]

    else:
        funkcija(line)


for niz in input_array:
    the_box_is_orange_actually(niz)
