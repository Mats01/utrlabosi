import sys

import pprint as pp

rijecnik_stanja = {}
abeceda = []
prihvatljiva_stanja = []
pocetno_stanje = None
ordering_alphabet = {}

liste_parova_stanja = {}


def parse_state(ulazni_niz):
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    lista_stanja = ulazni_niz.split(",")
    for i in lista_stanja:
        rijecnik_stanja[i] = []

    j = 0
    for i in lista_stanja:
        ordering_alphabet[i] = j
        j += 1


def parse_alphabet(ulazni_niz):
    global abeceda
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    abeceda = ulazni_niz.split(",")
    for i in rijecnik_stanja:
        for j in abeceda:
            rijecnik_stanja[i].append([])


def parse_acceptable(ulazni_niz):
    global prihvatljiva_stanja
    ulazni_niz = ulazni_niz[:-1]  # mice new line character
    prihvatljiva_stanja = ulazni_niz.split(",")


def funkcija(ulaz):

    try:
        ulaz = ulaz[:-1].split("->")
        lijevi_dio = ulaz[0]
        slijed_stanja = ulaz[1].split(",")
        tren_stanje, simbol = lijevi_dio.split(",")
        for slijedece_stanje in slijed_stanja:
            if slijedece_stanje != "#":
                rijecnik_stanja[tren_stanje][abeceda.index(
                    simbol)].append(slijedece_stanje)

    except:
        print("Problem!")


def init_staircase(length):
    rez = []
    for i in range(length):
        rez.append([])
        for j in range(length):
            if j < i:
                rez[i].append(0)
            else:
                rez[i].append(None)
    return rez


def generiraj_prazne_liste(dulijna_prihvatljivih):
    for i in range(dulijna_prihvatljivih):
        for j in range(i+1, dulijna_prihvatljivih):
            liste_parova_stanja[(i, j)] = []


def izracunaj_dohvatlijva(dohvatljiva_stanja):
    q = [pocetno_stanje]
    while q != []:
        curr = q.pop()
        if curr in dohvatljiva_stanja:
            continue
        dohvatljiva_stanja.append(curr)
        for stanje in rijecnik_stanja[curr]:
            stanje = stanje[0]
            if stanje not in q:
                q.append(stanje)


def izbaci_nedohvatljiva(dohvatljiva_stanja, nedohvatljiva):
    global prihvatljiva_stanja
    stanja = list(rijecnik_stanja.keys())
    for stanje in stanja:
        if stanje not in dohvatljiva_stanja:
            del rijecnik_stanja[stanje]
            nedohvatljiva.append(stanje)

    temp = []
    for stanje in prihvatljiva_stanja:

        if stanje in dohvatljiva_stanja:
            temp.append(stanje)
    prihvatljiva_stanja = temp


def prvi_prolaz(dohvatljiva_stanja, dulijna_prihvatljivih, staircase):

    for i in range(dulijna_prihvatljivih):
        for j in range(i, dulijna_prihvatljivih):

            if staircase[j][i] != None:
                prvi_p = dohvatljiva_stanja[i] in prihvatljiva_stanja
                drugi_p = dohvatljiva_stanja[j] in prihvatljiva_stanja
                if(prvi_p ^ drugi_p):
                    staircase[j][i] = 1


def rekurzivni_iksinator(pair):

    for par in liste_parova_stanja[pair]:
        if staircase[par[j]][par[i]] == 1:
            continue
        elif staircase[par[j]][par[i]] == 0:
            staircase[par[j]][par[i]] = 1
            rekurzivni_iksinator(par)


def algoritam(dohvatljiva_stanja, dulijna_prihvatljivih, staircase):

    for i in range(dulijna_prihvatljivih):
        for j in range(i+1, dulijna_prihvatljivih):
            if staircase[j][i] == 0:
                # liste_parova_stanja[(i,j)].append((1,3))
                prvi = list(rijecnik_stanja.values())[i]
                drugi = list(rijecnik_stanja.values())[j]
                iduci_par = ()
                for slovo in range(len(abeceda)):
                    try:
                        p_i = dohvatljiva_stanja.index(prvi[slovo][0])
                        p_j = dohvatljiva_stanja.index(drugi[slovo][0])

                    except ValueError:
                        continue
                    iduci_par = (
                        dohvatljiva_stanja[p_i], dohvatljiva_stanja[p_j])
                    iduci_par = sorted(iduci_par, key=lambda word: [
                        ordering_alphabet.get(word)])
                    p_i = dohvatljiva_stanja.index(iduci_par[0])
                    p_j = dohvatljiva_stanja.index(iduci_par[1])

                    if staircase[p_j][p_i] == 1:
                        staircase[j][i] = 1
                        rekurzivni_iksinator((i, j))
                    elif staircase[p_j][p_j] == 0:
                        if (i, j) not in liste_parova_stanja[(p_i, p_j)]:
                            liste_parova_stanja[(p_i, p_j)].append((i, j))


def gray_box():
    global rijecnik_stanja
    global prihvatljiva_stanja
    global pocetno_stanje
    nedohvatljiva = []

    # pokusajmo ispisati sva dohvatljiva stanja
    dohvatljiva_stanja = []
    izracunaj_dohvatlijva(dohvatljiva_stanja)

    # leksikografski sortiraj dohvatljiva stanja
    dohvatljiva_stanja = sorted(dohvatljiva_stanja, key=lambda word: [
                                ordering_alphabet.get(word)])
    dulijna_prihvatljivih = len(dohvatljiva_stanja)

    izbaci_nedohvatljiva(dohvatljiva_stanja, nedohvatljiva)

    # inicijaliziraj tablicu istovjetonosti
    staircase = init_staircase(dulijna_prihvatljivih)

    # prvi korak
    prvi_prolaz(dohvatljiva_stanja, dulijna_prihvatljivih, staircase)

    # rekurzivno oznacavanje
    generiraj_prazne_liste(dulijna_prihvatljivih)
    algoritam(dohvatljiva_stanja, dulijna_prihvatljivih, staircase)

    istovjetna = {}

    for i in range(dulijna_prihvatljivih):
        for j in range(i+1, dulijna_prihvatljivih):
            if staircase[j][i] == 0:
                istovjetna[dohvatljiva_stanja[j]] = dohvatljiva_stanja[i]

    temp = []
    for i in dohvatljiva_stanja:
        if i not in list(istovjetna.keys()):
            temp.append(i)
    dohvatljiva_stanja = temp

    temp = []

    for i in prihvatljiva_stanja:
        if i not in list(istovjetna.keys()):
            temp.append(i)
    prihvatljiva_stanja = temp

    kljucevi = list(istovjetna.keys())[::-1]
    
    for stanje in kljucevi:
        if pocetno_stanje == stanje:
            pocetno_stanje = istovjetna[stanje]    

    print(",".join(dohvatljiva_stanja))
    print(",".join(abeceda))
    print(",".join(prihvatljiva_stanja))
    print(pocetno_stanje)
    for i in dohvatljiva_stanja:

        prelazi = rijecnik_stanja[i]

        for j in range(len(abeceda)):
            output = ""
            output += i + "," + abeceda[j] + "->"
            next_stanje = prelazi[j][0]
            kljucevi = list(istovjetna.keys())[::-1]
         
            for stanje in kljucevi:
                if next_stanje == stanje:
                    next_stanje = istovjetna[stanje]
            
            output += next_stanje

            print(output)


line_number = 0
for line in sys.stdin:
    line_number += 1

    if line_number == 1:

        parse_state(line)

    elif line_number == 2:
        parse_alphabet(line)

    elif line_number == 3:
        parse_acceptable(line)

    elif line_number == 4:
        pocetno_stanje = line[:-1]

    else:
        if line[:-1] == 'q':
            gray_box()
            break
        funkcija(line)

gray_box()
