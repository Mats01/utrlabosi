#!/bin/bash
for i in {1..14} # broj ispitnih primjera
do
# generiraj ime direktorija s vodećom nulom
dir=$(printf "%0*d\n" 2 $i)
echo "Test $dir"
# pokreni program i provjeri izlaz
res=`python3 MinDka.py < primjeri/test$dir/t.ul | diff primjeri/test$dir/t.iz -`
if [ "$res" != "" ]
then
# izlazi ne odgovaraju
echo "FAIL"
echo $res
else
# OK!
echo "OK"
fi
done
